#ifndef _COMMON_SIGNAL_PRINT_H
#define _COMMON_SIGNAL_PRINT_H

#include <stdio.h>
#include "float.h"

void signal_print(FILE *fp, const char *prefix, const f32 *f, int n);

#endif
