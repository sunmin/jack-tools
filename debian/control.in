Source: jack-tools
Section: sound
Priority: optional
Maintainer: Debian Multimedia Maintainers <pkg-multimedia-maintainers@lists.alioth.debian.org>
Uploaders: Arnout Engelen <arnouten@bzzt.net>,
Build-Depends: @cdbs@,
 bzip2,
 flex,
 freeglut3-dev,
 libjack-dev,
 libasound2-dev,
 libsndfile1-dev,
 libsamplerate-dev,
 liblo-dev,
 libncurses-dev,
 libtool,
 libxext-dev,
 libxt-dev,
 asciidoc,
 xsltproc,
 libxml2-utils,
 docbook-xml,
 docbook-xsl
Standards-Version: 3.9.6
Vcs-Browser: http://anonscm.debian.org/gitweb/?p=pkg-multimedia/jack-tools.git
Vcs-Git: git://anonscm.debian.org/pkg-multimedia/jack-tools.git
Homepage: http://slavepianos.org/rd/?t=rju

Package: jack-tools
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, jackd
Description: various JACK tools: dl, record, scope, osc, plumbing, udp, play, transport
 jack-tools is a collection of small tools for the JACK Audio Connection
 Kit.
 .
 JACK allows the connection of multiple applications to an audio device,
 as well as allowing them to share audio between themselves.
 .
 jack-dl loads dsp algorithms from shared libraries.  Commands are sent
 as OSC packets over a UDP connection.
 .
 jack-record is a light-weight JACK capture client to write an arbitrary
 number of channels to disk.
 .
 jack-scope draws either a time domain signal trace or a self
 correlation trace.  Multiple input channels are superimposed, each
 channel is drawn in a different color.  jack-scope accepts OSC packets
 for interactive control of drawing parameters.
 .
 jack-osc publishes the transport state of the local JACK server as OSC
 packets over a UDP connection.  jack-osc allows any OSC enabled
 application to act as a JACK transport client, receiving sample
 accurate pulse stream timing data, and monitoring and initiating
 transport state change.
 .
 jack-plumbing maintains a set of port connection rules and manages
 these as clients register ports with JACK.  Port names are implicitly
 bounded regular expressions and support sub-expression patterns.
 .
 jack-udp is a UDP audio transport mechanism for JACK.  jack-udp is
 obsolete: use net driver instead.
 .
 jack-play is a light-weight JACK sound file player. It creates as many
 output ports as there are channels in the input file.
 .
 jack-transport is a JACK session manager.  It reads configuration
 information from a system wide and a user specific configuration file
 and manages sessions involving the JACK daemon proper and optionally a
 set of secondary jack daemons.
